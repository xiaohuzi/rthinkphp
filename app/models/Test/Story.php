<?php

/**
 * test库 story表的操作类
 */
class Test_StoryModel extends BaseModel
{

    protected static $_instance = null;

    protected static $_table = 'story';

    public function __construct()
    {
        $db_conf = RThink_Config::get('db.test');

        parent::__construct($db_conf);
    }

    public static function instance()
    {
        if (null == self::$_instance) {
            self::$_instance = new self();
        }

        self::$_instance->selectTable(self::$_table);

        return self::$_instance;
    }


}