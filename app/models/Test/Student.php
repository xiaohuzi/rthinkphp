<?php
/**
 * Created by PhpStorm.
 * User: zhangxiaohu01
 * Date: 15-3-13
 * Time: 上午11:31
 */

/**
 * Class StudentModel
 */
class Test_StudentModel extends MongoBaseModel {

    protected static $_instance = null;

    protected static $_col = 'students';

    public function __construct() {
        $conf = RThink_Config::get('mongodb');

        parent::__construct($conf);
    }

    public static function instance()
    {
        if (null == self::$_instance) {
            self::$_instance = new self();
        }

        self::$_instance->setCollection(self::$_col);

        return self::$_instance;
    }

}