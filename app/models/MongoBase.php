<?php

/**
 * Class MongoBaseModel
 */
abstract class MongoBaseModel extends MongoPHP
{
    protected static $_mongo = null;

    public function __construct(array $conf) {

        // 连接 Mongo
        parent::__construct(array('username' => $conf['username'], 'password' => $conf['password'], 'host' => $conf['host'] , 'port' => $conf['port']));
        parent::selectDB($conf['database']);

    }

}