<?php

/**
 * 用户使用向导 控制器
 *
 * /*
 * 查询条件示例
 *
 * #1. ? 与 bind数量匹配
 * $condition = array(
 * 'fields' => array('id', 'title', 'author', 'create_time'),
 * 'condition' => 'id in (?, ?) or title=?',
 * 'bind' => array(1, 2, 'c'),
 * 'limit' => array('count' => 5)
 * );
 *
 * #2. 全是and 操作 可简化为 - bind的数据自动进行qoute处理
 * $condition = array(
 * 'fields' => array('id', 'title', 'author', 'create_time'),
 * 'condition' => array('id' => 1, 'author' => 'Ronny'),
 * 'limit' => array('count' => 5)
 * );
 * #3. like 操作
 * $condition = array(
 * 'condition' => "username like ? and status = 1",
 * 'bind' => array("%php%")
 * );
 *
 * #4. 数量较多的 in 操作单独处理
 * $ids = array(1, 3, 2,4 ,5);
 * $condition = array(
 * 'condition' => Test_StoryModel::instance()->getDbInstance()->quoteInto('id in (?)', $ids),
 * );
 *
 * 注: ?db_debug=2 可以查看出错时的sql语句
 *
 * Class UserGuideController
 */
class Userguide_ListController extends RThink_Controller_Action
{
    public function indexAction()
    {
        $data = array();

        $data['title'] = '列表页面';

        #1. ? 与 bind数量匹配
        $condition = array(
        'fields' => array('id', 'title', 'author', 'create_time'),
        'condition' => 'id in (?, ?) or title=?',
        'bind' => array(1, 2, 'c'),
        'limit' => array('count' => 5)
        );

        $data['story_list'] = Test_StoryModel::instance()->fetchAll($condition);

//        $this->setInvokeArg('layout', 'mainLayout');

        $this->render($data, 'list');
    }

}