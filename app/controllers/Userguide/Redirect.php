<?php
/**
 * Created by PhpStorm.
 * User: zhangxiaohu01
 * Date: 15-5-28
 * Time: 下午4:00
 *
 * 用户使用向导 - 请求重定向
 *
 */

class Userguide_RedirectController extends RThink_Controller_Action {

    public function indexAction()
    {
        //将当前action重定向到 /userguide/list - 默认是302跳转
        $this->getResponse()->setHttpResponseCode(301);
        $this->_redirect('/userguide/list');
    }
}

