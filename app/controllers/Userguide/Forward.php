<?php
/**
 * Created by PhpStorm.
 * User: zhangxiaohu01
 * Date: 15-5-28
 * Time: 下午3:56
 *
 * 用户使用向导 - 请求转发
 */

class Userguide_ForwardController extends RThink_Controller_Action {

    public function indexAction() {

        // 将当前action的请求转发到 /usergide/list
        $this->_forward('index', 'list', 'userguide');
    }
}