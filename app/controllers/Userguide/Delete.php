<?php
/**
 * Created by PhpStorm.
 * User: zhangxiaohu01
 * Date: 15-5-28
 * Time: 下午3:46
 *
 * 用户使用向导 - 数据库 删除操纵
 *
 * $where = array(
     'id' => 6,
 *   'title' => 'php'
 * )
 *
 * 转化sql id = 6 and title = 'php'
 *
 * $where = array(
    'id > ?' => 6,
    'title = ?' => 'php',
);
 *  转化 sql id > 6 and title = 'php'
 *
 *  $where = array(
    'id in (?)' => array(7, 16),
    );
 *  转化 sql id (7, 16)
 * Class Userguide_UpdateController
 */



class Userguide_DeleteController extends RThink_Controller_Action {

    public function indexAction() {

        $where = array(
            'id' => 15,
            'title' => 'java',
        );

        $where = array(
            'id > ' => 15,
            'title = ?' => 'java',
        );

        $where = array(
            'id in (?)' => array(19,17),
        );

        $story_model = Test_StoryModel::instance();

        $res = $story_model->delete($where);


        var_dump($res);


    }
}