
<?php

/**
 * 操作mongodb 示例
 * Class Userguide_MongoController
 */
class Userguide_MongoController extends RThink_Controller_Action
{
    public function indexAction()
    {
        $data = array();


        $stu_model = Test_StudentModel::instance();

        $real_name = trim($this->getRequest()->getParam('real_name'));
        $nickname = trim($this->getRequest()->getParam('nickname'));
        $phone_number = trim($this->getRequest()->getParam('phone_number'));

        $where = array();

        if (!empty($real_name)) {
            $where['real_name'] = new MongoRegex("/". $real_name . "/");
        } else if (!empty($nickname)) {
            $where['nickname'] = new MongoRegex("/". $nickname . "/");
        } else if (!empty($phone_number)) {
            $where['phone_number'] = new MongoRegex("/". $phone_number . "/");
        }

//
//        示例：select('user',array('id','name'),array('id'=>1),array('num'=>1),10,2);
//     * 类似：select id,name from user where id=1 order by num asc limit 2,10;

        //查询
        $stu_list = $stu_model->select($where,  array(), array('_id' => -1), 10, 2);
/*
        //修改
        $res = $stu_model->update(
            array('serviceAssiantIdentityArray' => array($stu_model->cmd('each') => explode(',', $assiant_list))),
            array('_id'=> $id), 'push');

        //添加

        $data = array(
            'referId' => $res->{'$id'},//$res insert操作返回的结果,获取最新插入数据的id
            'created_at' => new MongoDate(), //mongo时间
        );

        $res = $stu_model->insert($data);

        //数组操作

        #1.删除数组元素所有字段
        $res = $stu_model->update($stu_model->getCol(),
            array('serviceCoachIdentityArray' => 1),
            array('_id' => $id), 'unset');

        #2.向数组元素中添加值,存在则不添加
        $res = $record_model->update(
            array('student_array' => $student_id),
            array('_id' => $record_id), 'addToSet');

        #3.删除数组元素字段中个一个值
        $res = $record_model->update(
        array('student_array' => $student_id),
        array('_id' => $record_id), 'pull');

*/


        var_dump($stu_list);

    }

}